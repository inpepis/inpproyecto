
import java.io.IOException;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

//import com.zcorp.util.Card;

/**
 * Clase Server: Servidor que recive a los jugadores del juego, imlplementa
 * operaciones para interactuar con los clientes
 * @author rich
 *
 */
public class Server {

  public static final short PORT = 4444;

	
  private ServerSocket server;
  
  Thread threadCA;
  
  
  
 
 
 
 
  public Server(short port) {
    try {
      server = new ServerSocket(port);
    } catch (IOException e) {
      System.err.println("Error al Crear el servidor\n"
             + "ERR I/O: " + e.getMessage());
      e.printStackTrace();
      System.exit(0);
    } 
  }
  
  
  
  
  
  /**
   * Inicia al servidor
   * @throws IOException
   */
  public void start() {
	
    threadCA = new Thread(new Runnable() {
      public void run() {
        try {
          while(true){
            ClientSocket aClientConection = new  ClientSocket(server.accept());
            System.out.println("> CONECTADO.");
          }
        } catch(IOException e) {
          System.err.println("Err No se pudo iniciar el servidor");
          System.exit(0);
        }
      }
    });
    
    
    threadCA.start();
 
    
    /*try {
      threadCA.join();
    } catch (InterruptedException e) {
      System.err.println("servidor interrumpido mientras esperaba conexiones");
      e.printStackTrace();
    }*/
  }
  
  
  
  
  
  private void sendDataClient(ClientSocket c, Serializable message){
    c.sendData(message);
  }
  
  /**
   * Envia mensajes al cliente c 
   * @param c
   * @param message
   */
  public void sedDataToClient(int id, Serializable message) {
    // implementar con un for para buscar al cliente respectivo dado un id
    // ya encontrado el cliente enviarle con sendDataForClient()
  } 
  
  
  
  
  /**
   * Lee mensajes del cliente c
   * @param c
   * @return
   */
  private Object readDataClient(ClientSocket c) {
    return c.readData();
  }
  
  public Object readDataFromClient(int id) {
    // implementar con un for para buscar al cliente respectivo dado un id
    // ya encontrado el cliente enviarle con readDataClient()
    return readDataClient(null); 
  }
  
  
  
  
  private static class ClientSocket {
  
    private Socket socket;
    
    private ObjectOutputStream out;
    private ObjectInputStream in; 
    
    private int id;
    
    
    
    
  
    public ClientSocket(Socket s) {
      try {
        socket = s;
        out = new ObjectOutputStream(s.getOutputStream());
        in =  new ObjectInputStream(s.getInputStream()); 
      } catch (IOException e) {
        System.err.println("Error al estableces los canales de comunicacion (i/O) con el cliente");
      }
    }
    
    
    
    
    public void sendData(Serializable s) {
      try {
        out.writeObject(s);
        out.flush();
      } catch (IOException e) {
        System.err.println("Err Conexion al Cliente Perdida: Imposible Escritura de Datos");
      }
    }
    
    
    public Object readData() {
      try {
        return in.readObject();
      } catch (ClassNotFoundException e) {
        System.err.println("Err class not found: " + e.getMessage());
        //e.printStackTrace();
        return null;
      } catch (IOException e) {
        System.err.println("Err Conexion al Cliente Perdida: Imposible Lectura de Datos");
        //e.printStackTrace();
        return null;
      }
    }
  
  } 
  
  public static void main(String[] args) {
    Server s = new Server(Server.PORT);
    s.start();
    System.out.println("Servidor Iniciado");
  }
  
}
