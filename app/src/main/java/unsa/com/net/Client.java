package unsa.com.net;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.InetSocketAddress;
import java.net.Socket;


/**
 * Clase del cliente implementa las operaciones necesarias para
 * la conexion con el servidor y operaciones para interactuar con el
 * @author Richard Z.
 *
 */
public class Client {

    private Socket client;
    private short port;

    private String host;

    private ObjectOutputStream out;
    private ObjectInputStream in;




    /**
     * Constructor que define los parametros para la conexion a un servidor
     * @param host direccion de servidor
     * @param port puerto de conexion
     */
    public Client(String host, short port) {
        this.host = host;
        this.port = port;
        client = new Socket();
    }




    /**
     * Inicia la conexion al servidor con los parametros ya definidos por el constructor
     * @return {@code true} si la conexion fue exitosa, {@code false} en otro caso
     */
    public void start() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    client.connect(new InetSocketAddress(host, port));
                    out = new ObjectOutputStream(client.getOutputStream());
                    in =  new ObjectInputStream(client.getInputStream());
                } catch (IOException e) {
                    //e.printStackTrace();
                    System.err.println("Err Client: No se pudo conectar a " + host);
                }
            }
        });
    }





    /**
     * Envia el mensaje @s ( @Serializale ) al servidor al que se conecto el cliente
     * @param s
     */
    public void sendData(Serializable s) {
        try {
            out.writeObject(s);
            out.flush();
        } catch (IOException e) {
            System.err.println("Err Conexion al Servidor Perdida: Imposible Escritura de Datos");
        }
    }





    /**
     * Lee mensajes ( @Object ) provenientes del servidor, si ocurre errores retorna @null
     * @return
     */
    public Object readData() {
        try {
            return in.readObject();
        } catch (ClassNotFoundException e) {
            System.err.println("Err class not found: " + e.getMessage());
            //e.printStackTrace();
            return null;
        } catch (IOException e) {
            System.err.println("Err Conexion al Servidor Perdida: Imposible Lectura de Datos");
            //e.printStackTrace();
            return null;
        }
    }
}