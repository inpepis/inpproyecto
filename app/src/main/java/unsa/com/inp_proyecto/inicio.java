package unsa.com.inp_proyecto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Inicio extends AppCompatActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

    }

    public void hacerDenuncia(View view) {

        Intent intent = new Intent(this,Main.class);
        startActivity(intent);
        Toast.makeText(this,"Llene el formulario",Toast.LENGTH_LONG).show();
    }
    public void verComisarias(View view) {

        Intent intent = new Intent(this,Comisarias.class);
        startActivity(intent);
        Toast.makeText(this,"Revise las comisarias asociadas ",Toast.LENGTH_LONG).show();
    }
    public void verMunicipalidades(View view) {

        Intent intent = new Intent(this,Comisarias.class);
        startActivity(intent);
        Toast.makeText(this,"Revise las municipalidades asociadas",Toast.LENGTH_LONG).show();
    }
    public void verContactanos(View view) {

        Intent intent = new Intent(this,Comisarias.class);
        startActivity(intent);
        Toast.makeText(this,"Comuniquese con nosotros si hubiera algun error",Toast.LENGTH_LONG).show();
    }

}
