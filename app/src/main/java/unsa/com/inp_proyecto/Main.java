package unsa.com.inp_proyecto;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import unsa.com.net.Client;

/**
 *Clase principal del Proyecto
 */
public class Main extends AppCompatActivity {

    public static final short PORT = 4444;
    public static final String SERVER = "192.168.8.102";


    private static Spinner sp_tipo_denu;
    private static Spinner sp_distrito;


    private int TAKE_PHOTO_CODE;
    private static int count;
    final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + "/Camera/";
    File newdir = new File(dir);



    /*********************************************/
    double latitud;
    double longitud;
    boolean gps_activo;
    Location location;
    LocationManager locationMngr;


    /*********************************************/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        ActionBar actionBar= getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


        String[] tipos_denu = {"DESCONOCIDO",
                "RECLAMOS",
                "AUTORIDADES",
                "CALLES",
                "VIVIENDAS",
                "OTROS"};


        sp_tipo_denu = (Spinner) findViewById(R.id.sp_tipo_denu);
        ArrayAdapter<String> adapter_tipo_denu = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, tipos_denu);
        sp_tipo_denu.setAdapter(adapter_tipo_denu);
        sp_tipo_denu.setOnItemSelectedListener(new Sp_Tipo_Denu());



        locationMngr = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        gps_activo = locationMngr.isProviderEnabled(locationMngr.GPS_PROVIDER);


        if ( ContextCompat.checkSelfPermission( this, android.Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {

            ActivityCompat.requestPermissions( this, new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION  }, 0 );
        }

        locationMngr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 3, new MyLocationListener());
    }


    public void conectar(View v) {
        TelephonyManager tMgr = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        String myID = tMgr.getDeviceId();

        Toast.makeText(this,"Su denuncia a sido procesada",Toast.LENGTH_LONG).show();


        //EditText et = (EditText)findViewById(R.id.text_ip);
        //Client c = new Client(et.getText().toString(), PORT);
        //c.start();

    }

    public void tomarFoto(View v) {
        count++;
        String file = dir+"denun_"+count+".jpg";
        File newfile = new File(file);
        try {
            newfile.createNewFile();
        }
        catch (IOException e)
        {
        }

        Uri outputFileUri = Uri.fromFile(newfile);

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

        startActivityForResult(cameraIntent, TAKE_PHOTO_CODE);
    }






    private class Sp_Tipo_Denu extends Activity implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) { // pos es cero en el primer item
            //u_medida_selected = pos;

        }

        public void onNothingSelected(AdapterView<?> parent) { }
    }

    private class Sp_Distrito extends Activity implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            //marca_selected = pos;
        }

        public void onNothingSelected(AdapterView<?> parent) { }
    }





    private class MyLocationListener implements LocationListener {

        private EditText et;


        public  MyLocationListener() {
            super();
            et = (EditText)findViewById(R.id.et_denuncia);

        }

        @Override
        public void onLocationChanged(Location location) {

            location = locationMngr.getLastKnownLocation(locationMngr.GPS_PROVIDER);
            latitud = location.getLatitude();
            longitud = location.getLongitude();

            et.setText(latitud + ", " + longitud);

            //System.out.println("\n\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  " + latitud + ", " + longitud + "\n\n\n");
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    }
}
