-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema denuncias
-- -----------------------------------------------------
-- 
-- 
-- 

-- -----------------------------------------------------
-- Schema denuncias
--
-- 
-- 
-- 
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `denuncias` DEFAULT CHARACTER SET utf8 ;
USE `denuncias` ;

-- -----------------------------------------------------
-- Table `denuncias`.`CIUDADANO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `denuncias`.`CIUDADANO` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `DNI` VARCHAR(8) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `denuncias`.`DISTRITO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `denuncias`.`DISTRITO` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `DISTRITO` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `denuncias`.`TIPO_DENUN`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `denuncias`.`TIPO_DENUN` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `TIPO` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `denuncias`.`DENUNCIA`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `denuncias`.`DENUNCIA` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `CIUDADANO` INT NOT NULL,
  `DENUN` LONGTEXT NOT NULL,
  `TIPO_DENUN` INT NOT NULL,
  `DISTRITO` INT NOT NULL,
  `DIRECCION` LONGTEXT NULL,
  `FOTO_REF` LONGTEXT NULL,
  `COORXY` POINT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_DENUNCIA_CIUDADANO_idx` (`CIUDADANO` ASC),
  INDEX `fk_DENUNCIA_DISTRITO1_idx` (`DISTRITO` ASC),
  INDEX `fk_DENUNCIA_TIPO_DENUN1_idx` (`TIPO_DENUN` ASC),
  CONSTRAINT `fk_DENUNCIA_CIUDADANO`
    FOREIGN KEY (`CIUDADANO`)
    REFERENCES `denuncias`.`CIUDADANO` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_DENUNCIA_DISTRITO1`
    FOREIGN KEY (`DISTRITO`)
    REFERENCES `denuncias`.`DISTRITO` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_DENUNCIA_TIPO_DENUN1`
    FOREIGN KEY (`TIPO_DENUN`)
    REFERENCES `denuncias`.`TIPO_DENUN` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


insert into TIPO_DENUN(TIPO) values('DESCONOCIDO');
insert into TIPO_DENUN(TIPO) values('RECLAMOS');
insert into TIPO_DENUN(TIPO) values('AUTORIDADES');
insert into TIPO_DENUN(TIPO) values('CALLES');
insert into TIPO_DENUN(TIPO) values('VIVIENDAS');
insert into TIPO_DENUN(TIPO) values('OTRO');


insert into DISTRITO(DISTRITO) values('DESCONOCIDO');
insert into DISTRITO(DISTRITO) values('AREQUIPA');
insert into DISTRITO(DISTRITO) values('ATO SELVA ALEGRE');
insert into DISTRITO(DISTRITO) values('CERRO COLORADO');
insert into DISTRITO(DISTRITO) values('JOSE LUIS BUSTAMANTE');
insert into DISTRITO(DISTRITO) values('SOCABAYA');
insert into DISTRITO(DISTRITO) values('YANAHUARA');
insert into DISTRITO(DISTRITO) values('CAYMA');
insert into DISTRITO(DISTRITO) values('HUNTER');
insert into DISTRITO(DISTRITO) values('PAUCARPATA');
insert into DISTRITO(DISTRITO) values('CONO NORTE');


insert into CIUDADANO(DNI) values('ANONIMO');
insert into CIUDADANO(DNI) values('34567890');
insert into CIUDADANO(DNI) values('67895432');
insert into CIUDADANO(DNI) values('23445876');
insert into CIUDADANO(DNI) values('58654344');
insert into CIUDADANO(DNI) values('54688743');
insert into CIUDADANO(DNI) values('53468745');
insert into CIUDADANO(DNI) values('75323453');
insert into CIUDADANO(DNI) values('34567373');
insert into CIUDADANO(DNI) values('54347895');
insert into CIUDADANO(DNI) values('23765334');
insert into CIUDADANO(DNI) values('45335456');

